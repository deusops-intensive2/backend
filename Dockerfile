#
# Build stage
#
FROM maven:3.6.0-jdk-11-slim AS build
COPY src /home/app/src
COPY pom.xml /home/app
RUN mvn -f /home/app/pom.xml clean install -Dmaven.test.skip=true

#
# Package stage
#
FROM openjdk:11-jre
COPY --from=build /home/app/target/studentsystem-0.0.1-SNAPSHOT.jar /usr/local/lib/studentsystem.jar
EXPOSE 8080
ENTRYPOINT ["java","-jar","/usr/local/lib/studentsystem.jar"]